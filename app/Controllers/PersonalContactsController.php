<?php

namespace app\Controllers;

use app\Interfaces\ContactsControllerInterface;
use app\Lib\Request;
use app\Lib\Response;
use app\Lib\Responses\JsonResponse;
use app\Lib\Responses\ViewResponse;
use app\Models\PersonalContact;

/**
 * Class PersonalContactsController
 *
 * @package app\Controllers
 */
class PersonalContactsController implements ContactsControllerInterface
{
    /**
     * Response
     *
     * @var Response $response
     */
    private Response $response;

    /**
     * PersonalContactsController constructor.
     */
    public function __construct()
    {
        $this->response = new Response();
    }
    
    /**
     * Get contacts
     *
     * @param Request $request
     * @return JsonResponse
     * @see ContactsControllerInterface::getContacts()
     */
    public function getContacts(Request $request): JsonResponse
    {
        $count = PersonalContact::getCount(
            $request->post('search')['value']
        );

        return  $this->response->json([
            'draw' => $request->post('draw', 0),
            'recordsTotal' => $count,
            'recordsFiltered' => $count,
            'data' => $contacts = PersonalContact::get(
                $request->post('search')['value'],
                $request->post('start', 0),
                $request->post('length', 10),
                $request->post('columns')[
                $request->post('order')[0]['column']
                ]['data'],
                $request->post('order')[0]['dir']
            ),
            'test' => $request->post('columns')[
                $request->post('order')[0]['column']
            ]['data']
        ]);
    }

    /**
     * Delete contact
     *
     * @param Request $request
     * @return JsonResponse
     * @see ContactsControllerInterface::deleteContact()
     */
    public function deleteContact(Request $request): JsonResponse
    {
        return  $this->response->json([
            'delete' => PersonalContact::delete(
                $request->post('id', 0)
            )
        ]);
    }

    /**
     * Edit contact
     *
     * @param Request $request
     * @return ViewResponse
     * @see ContactsControllerInterface::editContact()
     */
    public function editContact(Request $request): ViewResponse
    {
        return  $this->response->view('personal_contacts/personal_contact_add_edit.php', [
            'isEdit' => true,
            'contact' => PersonalContact::find(
                $request->get('contact', 0)
            )
        ]);
    }

    /**
     * Add or update contact
     *
     * @param Request $request
     * @return JsonResponse
     * @see ContactsControllerInterface::storeContact()
     */
    public function storeContact(Request $request): JsonResponse
    {
        $id = $request->post('id', 0);

        if ($id && PersonalContact::find($id)) {
            return $this->response->json([
                'store' => PersonalContact::update(
                    $id,
                    $request->postAll()
                )
            ]);
        } else {
            return $this->response->json([
                'store' => PersonalContact::insert(
                    $request->postAll()
                )
            ]);
        }
    }
}