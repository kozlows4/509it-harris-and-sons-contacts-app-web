<?php

namespace app\Controllers;

use app\Interfaces\ContactsControllerInterface;
use app\Lib\Request;
use app\Lib\Response;
use app\Lib\Responses\JsonResponse;
use app\Lib\Responses\ViewResponse;
use app\Models\BusinessContact;
use app\Models\Model;

/**
 * Class BusinessContactsController
 *
 * @package app\Controllers
 */
class BusinessContactsController implements ContactsControllerInterface
{
    /**
     * Response
     *
     * @var Response $response
     */
    private Response $response;

    /**
     * BusinessContactsController constructor.
     */
    public function __construct()
    {
        $this->response = new Response();
    }

    /**
     * Get contacts
     *
     * @param Request $request
     * @return JsonResponse
     * @see ContactsControllerInterface::getContacts()
     */
    public function getContacts(Request $request): JsonResponse
    {
        $count = BusinessContact::getCount(
            $request->post('search')['value']
        );

        return $this->response->json([
            'draw' => $request->post('draw', 0),
            'recordsTotal' => $count,
            'recordsFiltered' => $count,
            'data' => $contacts = BusinessContact::get(
                $request->post('search')['value'],
                $request->post('start', Model::OFFSET),
                $request->post('length', Model::LIMIT),
                $request->post('columns')[
                $request->post('order')[0]['column']
                ]['data'],
                $request->post('order')[0]['dir']
            )
        ]);
    }

    /**
     * Delete contact
     *
     * @param Request $request
     * @return JsonResponse
     * @see ContactsControllerInterface::deleteContact()
     */
    public function deleteContact(Request $request): JsonResponse
    {
        return $this->response->json([
            'delete' => BusinessContact::delete(
                $request->post('id', 0)
            )
        ]);
    }

    /**
     * Edit contact
     *
     * @param Request $request
     * @return ViewResponse
     * @see ContactsControllerInterface::editContact()
     */
    public function editContact(Request $request): ViewResponse
    {
        return $this->response->view('business_contacts/business_contact_add_edit.php', [
            'isEdit' => true,
            'contact' => BusinessContact::find(
                $request->get('contact', 0)
            )
        ]);
    }

    /**
     * Add or update contact
     *
     * @param Request $request
     * @return JsonResponse
     * @see ContactsControllerInterface::storeContact()
     */
    public function storeContact(Request $request): JsonResponse
    {
        $id = $request->post('id', 0);

        if ($id && BusinessContact::find($id)) {
            return $this->response->json([
                'store' => BusinessContact::update(
                    $id,
                    $request->postAll()
                )
            ]);
        } else {
            return $this->response->json([
                'store' => BusinessContact::insert(
                    $request->postAll()
                )
            ]);
        }
    }
}