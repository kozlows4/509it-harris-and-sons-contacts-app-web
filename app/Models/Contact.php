<?php


namespace app\Models;

/**
 * Class Contact
 * @package app\Models
 */
abstract class Contact extends Model
{
    /**
     * Common searchable columns
     *
     * @var string[] $searchable
     * @see Model::$searchable
     */
    protected static array $searchable = [
        'first_name',
        'last_name',
        'email',
        'phone_area_code',
        'phone_number',
        'address_line_1',
        'address_line_2',
        'address_line_3',
        'postcode',
        'city',
        'country',
    ];

    /**
     * Common sortable columns
     *
     * @var string[] $orderable
     * @see Model::$orderable
     */
    protected static array $orderable = [
        'id',
        'first_name',
        'last_name',
        'email'
    ];

    /**
     * Common nullable columns
     *
     * @var array|string[] $nullable
     * @see Model::$nullable
     */
    protected static array $nullable = [
        'address_line_2',
        'address_line_3'
    ];
}