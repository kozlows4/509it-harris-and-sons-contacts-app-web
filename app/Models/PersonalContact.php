<?php


namespace app\Models;


/**
 * Class PersonalContact
 * @package app\Models
 */
class PersonalContact extends Contact
{
    /**
     * @var string $table
     * @see Model::$table
     */
    protected static string $table = 'personal_contacts';

    /**
     * @var array|string[] $searchable
     * @see Contact::$searchable
     */
    protected static array $searchable = [
        'home_phone_area_code',
        'home_phone_number'
    ];

    /**
     * Get list of searchable columns
     *
     * @return array
     */
    protected static function getSearchableColumns(): array
    {
        return array_merge(self::$searchable, parent::$searchable);
    }

    /**
     *  Get the name of the sorting column
     *
     * @param string $orderBy
     * @return string
     */
    protected static function getOrderByColumn(string $orderBy): string
    {
        return in_array(
            $orderBy = strtolower($orderBy),
            $orderable = array_merge(
                self::$orderable,
                parent::$orderable
            )
        ) ? $orderBy : self::$key;
    }
}