<?php


namespace app\Models;

use app\Lib\DB;

/**
 * Class Model
 * @package app\Models
 */
abstract class Model
{
    /**
     * Limit results
     */
    public const LIMIT = 10;

    /**
     * Offset results
     */
    public const OFFSET = 0;

    /**
     * Order ascending
     */
    private const ORDER_ASC = 'ASC';

    /**
     * Order descending
     */
    private const ORDER_DESC = 'DESC';

    /**
     * Name of the table used by queries
     *
     * @var string $table
     */
    protected static string $table = '';

    /**
     * Name of the primary key column in the table, usually "id"
     *
     * @var string $key
     */
    protected static string $key = 'id';

    /**
     * List of columns that may store NULL value
     *
     * @var string[] $nullable
     */
    protected static array $nullable = [];

    /**
     * List of columns that may be used to filter the records
     *
     * @var string[] $nullable
     */
    protected static array $searchable = [];

    /**
     * List of columns that may be used to sort the records
     *
     * @var string[] $orderable
     */
    protected static array $orderable = [];

    /**
     * Delete record from the database
     *
     * @param int $id
     * @return bool
     */
    public static function delete(int $id): bool
    {
        return !!DB::query(
            'DELETE FROM ' . static::$table . ' WHERE ' . static::$key . ' = :id;',
            [
                'id' => $id
            ]
        );
    }

    /**
     * Get multiple records from the database
     *
     * @param string $search
     * @param int $offset
     * @param int $limit
     * @param string $orderColumn
     * @param string $orderDirection
     * @return array
     */
    public static function get(
        string $search,
        int $offset,
        int $limit,
        string $orderColumn,
        string $orderDirection
    ): array
    {
        [
            'searchString' => $searchString,
            'placeholders' => $searchPlaceholders
        ] = self::getSearchStringAndPlaceholders($search);

        $result = DB::query(
            'SELECT * FROM '
                . static::$table
                . $searchString
                . ' ORDER BY '
                . static::getOrderByColumn($orderColumn)
                . ' '
                . self::getOrderingString($orderDirection)
                . ' LIMIT :limitValue OFFSET :offsetValue;',
            array_merge(
                $searchPlaceholders,
                [
                    'offsetValue' => $offset,
                    'limitValue' => $limit
                ]
            )
        );

        if ($result) {
            return $result->fetchAll();
        } else {
            return [];
        }
    }

    /**
     * Get number of records matching the search criteria
     *
     * @param string $search
     * @return int
     */
    public static function getCount(string $search = ''): int
    {
        [
            'searchString' => $searchString,
            'placeholders' => $searchPlaceholders
        ] = self::getSearchStringAndPlaceholders($search);

        $result = DB::query(
            'SELECT COUNT(' . static::$key . ') AS count FROM '
            . static::$table
            . $searchString,
            $searchPlaceholders
        );

        return $result
            ? (
                $result->fetch()['count'] ?? 0
            )
            : 0;
    }

    /**
     * Get a single record from teh database
     *
     * @param int $id
     * @return array|null
     */
    public static function find(int $id): ?array
    {
        $result = DB::query(
            'SELECT * FROM ' . static::$table . ' WHERE ' . static::$key . ' = :id LIMIT 1;',
            [
                'id' => $id
            ]
        )->fetch();

        if ($result) {
            return $result;
        } else {
            return null;
        }
    }

    /**
     * Add new record to the database
     *
     * @param array $params
     * @return bool
     */
    public static function insert(array $params): bool
    {
        $values = self::joinValues(
            $keys = array_keys(
                $params = self::setNullValues($params)
            )
        );
        $keys = self::joinValues(
            camelCaseToSnakeCaseArray($keys),
            false
        );

        return !!DB::query(
            'INSERT INTO ' . static::$table . ' (' . $keys . ') VALUES ('. $values .');',
            $params
        );
    }

    /**
     * Update existing record in the database
     *
     * @param int $id
     * @param array $params
     * @return bool
     */
    public static function update(int $id, array $params): bool
    {
        return !!DB::query(
            'UPDATE ' . static::$table . ' SET ' . self::joinKeyValuePairs($params, [static::$key]) . ' WHERE ' . static::$key . ' = :id;',
            array_merge(
                [
                    'id' => $id
                ],
                self::setNullValues($params)
            )
        );
    }

    /**
     * Format part of the INSERT query string
     *
     * @param array $values
     * @param bool $addColons
     * @param string $glue
     * @return string
     */
    private static function joinValues(array $values, bool $addColons = true, string $glue = ', '): string
    {
        if ($addColons) {
            foreach ($values as &$value) {
                $value = ':' . $value;
            }
        }

        return implode($glue, $values);
    }

    /**
     * Format part of the UPDATE query string
     *
     * @param array $params
     * @param array $excludeKeys
     * @param string $glue
     * @return string
     */
    private static function joinKeyValuePairs(array $params, array $excludeKeys = [], string $glue = ', '): string
    {
        $joinedKeyValuePairs = [];

        foreach ($params as $key => $value) {
            if (in_array($key, $excludeKeys)) {
                continue;
            }

            $joinedKeyValuePairs[] = camelCaseToSnakeCase($key) . '= :' . $key;
        }

        return implode($glue, $joinedKeyValuePairs);
    }

    /**
     * Replace empty strings with NULL values where applicable
     *
     * @param array $params
     * @return array
     */
    private static function setNullValues(array $params): array
    {
        foreach (snakeCaseToCamelCaseArray(static::$nullable) as $item) {
            if (!array_key_exists($item, $params)) {
                continue;
            }

            if (strlen($params[$item]) < 1) {
                $params[$item] = null;
            }
        }

        return $params;
    }

    /**
     * Get ORDER BY direction
     *
     * @param string $order
     * @return string
     */
    private static function getOrderingString(string $order): string
    {
        $order = strtoupper($order);

        if ($order === self::ORDER_ASC) {
            return self::ORDER_ASC;
        } elseif ($order === self::ORDER_DESC) {
            return self::ORDER_DESC;
        } else {
            return self::ORDER_ASC;
        }
    }

    /**
     * Format search string of the SELECT query
     *
     * @param string $search
     * @return array
     */
    private static function getSearchStringAndPlaceholders(string $search): array
    {
        if (strlen($search) < 1) {
            return [
                'searchString' => '',
                'placeholders' => []
            ];
        }

        $placeholders = [];
        $searchString = ' WHERE ';
        $searchableColumns = static::getSearchableColumns();

        for ($i = 0, $j = count($searchableColumns); $i < $j;) {
            if ($i === 0) {
                $searchString .= ($searchableColumns[$i] . ' LIKE CONCAT("%", :search' . ++$i . ', "%")');
                $placeholders['search' . $i] = $search;
            } else {
                $searchString .= (' OR ' . $searchableColumns[$i] . ' LIKE CONCAT("%", :search' . ++$i . ', "%")');
                $placeholders['search' . $i] = $search;
            }
        }

        return [
            'searchString' => $searchString,
            'placeholders' => $placeholders
        ];
    }

    /**
     * Get list of searchable columns
     *
     * @return array
     */
    protected abstract static function getSearchableColumns(): array;

    /**
     * Get the name of the sorting column
     *
     * @param string $orderBy
     * @return string
     */
    protected abstract static function getOrderByColumn(string $orderBy): string;
}