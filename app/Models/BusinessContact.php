<?php


namespace app\Models;


/**
 * Class BusinessContact
 * @package app\Models
 */
class BusinessContact extends Contact
{
    /**
     * @var string $table
     * @see Model::$table
     */
    protected static string $table = 'business_contacts';

    /**
     * @var string[] $searchable
     * @see Contact::$searchable
     */
    protected static array $searchable = [
        'business_phone_area_code',
        'business_phone_number',
        'company',
        'position'
    ];

    /**
     * @var string[] $orderable
     * @see Contact::$orderable
     */
    protected static array $orderable = [
        'company',
        'position'
    ];

    /**
     * Get list of searchable columns
     *
     * @return array
     */
    protected static function getSearchableColumns(): array
    {
        return array_merge(self::$searchable, parent::$searchable);
    }

    /**
     * Get the name of the sorting column
     *
     * @param string $orderBy
     * @return string
     */
    protected static function getOrderByColumn(string $orderBy): string
    {
        return in_array(
            $orderBy = strtolower($orderBy),
            $orderable = array_merge(
                self::$orderable,
                parent::$orderable
            )
        ) ? $orderBy : self::$key;
    }
}