<?php

use app\Lib\Config;
use app\Lib\Log;
use app\Lib\Response;
use app\Lib\Responses\ViewResponse;
use app\Lib\Router;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

if (!function_exists('__')) {
    /**
     * Display value
     *
     * @param mixed $data
     */
    function __($data): void
    {
        echo htmlspecialchars($data);
    }
}

if (!function_exists('mix')) {
    /**
     * Get asset path
     *
     * @param string $asset
     * @return string
     */
    function mix(string $asset): string
    {
        try {
            $manifestPath = realpath('../public/mix-manifest.json');

            if (empty($manifestPath) || !file_exists($manifestPath)) {
                throw new Exception("The MIX manifest doesn't exist");
            }

            $manifest = json_decode(
                file_get_contents($manifestPath),
                true
            );

            if (
            array_key_exists(
                $key = (substr($asset,0, 1) === '/')
                    ? $asset
                    : '/' . $asset,
                $manifest
            )
            ) {
                return getWebProtocol() . url() . $manifest[$key];
            } else {
                throw new Exception("Asset '". $asset ."' doesn't exist");
            }
        } catch (Exception $exception) {
            Log::errorLog($exception->getMessage());

            return '';
        }
    }
}

if (!function_exists('url')) {
    /**
     * Get url
     *
     * @return string
     */
    function url(): string
    {
        return $_SERVER['HTTP_HOST'];
    }
}

if (!function_exists('route')) {
    /**
     * Get route
     *
     * @param string $route
     * @param array $parameters
     * @return string
     */
    function route(string $route, array $parameters = []): string
    {
        $route = getWebProtocol()
            . $_SERVER['HTTP_HOST']
            . (Router::findRoute($route) ?? ('/' . $route));

        $isFirstIteration = true;
        $params = '';

        foreach ($parameters as $key => $value) {
            if ($isFirstIteration) {
                $params .= '?';

                $isFirstIteration = false;
            } else {
                $params .= '&';
            }

            $params .= ($key . '=' . $value);
        }

        return $route . $params;
    }
}

if (!function_exists('isHttps')) {
    /**
     * Check if HTTPS is used
     *
     * @return bool
     */
    function isHttps(): bool
    {
        return isset($_SERVER['HTTPS']);
    }
}

if (!function_exists('getWebProtocol')) {
    /**
     * Check if HTTPS is used
     *
     * @return string
     */
    function getWebProtocol(): string
    {
        return (isHttps() ? 'https' : 'http') . '://';
    }
}

if (!function_exists('camelCaseToSnakeCase')) {
    /**
     * Convert string from camel case to snake case
     *
     * @param string $input
     * @return string
     */
    function camelCaseToSnakeCase(string $input): string
    {
        return strtolower(
            preg_replace('/\B([A-Z0-9])/', '_$1', $input)
        );
    }
}

if (!function_exists('snakeCaseToCamelCase')) {
    /**
     * Convert string from snake case to camel case
     *
     * @param string $input
     * @return string
     */
    function snakeCaseToCamelCase(string $input): string
    {
        return preg_replace_callback('/_([a-z0-9]?)/', function ($match) {
            return strtoupper($match[1]);
        }, $input);
    }
}

if (!function_exists('camelCaseToSnakeCaseArray')) {
    /**
     * Convert array of strings from camel case to snake case
     *
     * @param string[] $input
     * @return string[]
     */
    function camelCaseToSnakeCaseArray(array $input): array
    {
        foreach ($input as &$item) {
            $item = camelCaseToSnakeCase($item);
        }

        return $input;
    }
}

if (!function_exists('snakeCaseToCamelCaseArray')) {
    /**
     * Convert array of strings from snake case to camel case
     *
     * @param string[] $input
     * @return string[]
     */
    function snakeCaseToCamelCaseArray(array $input): array
    {
        foreach ($input as &$item) {
            $item = snakeCaseToCamelCase($item);
        }

        return $input;
    }
}