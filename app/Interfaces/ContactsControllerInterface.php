<?php


namespace app\Interfaces;


use app\Lib\Request;
use app\Lib\Responses\JsonResponse;
use app\Lib\Responses\ViewResponse;

/**
 * Interface ContactsControllerInterface
 *
 * @package app\Interfaces
 */
interface ContactsControllerInterface
{
    /**
     * Get list of contacts
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getContacts(Request $request): JsonResponse;

    /**
     * Delete contact
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function deleteContact(Request $request): JsonResponse;

    /**
     * Edit contact
     *
     * @param Request $request
     * @return ViewResponse
     */
    public function editContact(Request $request): ViewResponse;

    /**
     * Add or update contact
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function storeContact(Request $request): JsonResponse;
}