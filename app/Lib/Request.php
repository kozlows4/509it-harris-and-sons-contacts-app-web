<?php


namespace app\Lib;

/**
 * Class Request
 *
 * @package app\Lib
 *
 * @method mixed|null get(string $key, ?mixed $default = null)
 * @method mixed|null post(string $key, ?mixed $default = null)
 * @method array|mixed[] getAll()
 * @method array|mixed[] postAll()
 */
class Request
{
    /**
     * @const
     */
    public const GET = 'get';

    /**
     * @const
     */
    public const POST = 'post';

    /**
     * @const
     */
    private const ALLOWED_METHODS = [
        self::GET,
        self::POST
    ];

    /**
     * @var array $getParams
     */
    private array $getParams;

    /**
     * @var array $postParams
     */
    private array $postParams;

    /**
     * @var string $requestMethod
     */
    private string $requestMethod;

    /**
     * Request constructor.
     *
     * @param string $method
     * @param array $parameters
     */
    public function __construct(string $method = '', array $parameters = [])
    {
        if (
            strlen($method) > 0
            && in_array(
                $method = strtolower(
                    trim($method)
                ),
                self::ALLOWED_METHODS
            )
        ) {
            $this->{$method . 'Params'} = $parameters;

            $this->requestMethod = $method;
        } else {
            $this->getParams = $_GET;
            $this->postParams = $_POST;

            $this->requestMethod = strtolower(
                trim($_SERVER['REQUEST_METHOD'])
            );
        }
    }

    /**
     * Get object properties
     *
     * @param $name
     * @param $arguments
     * @return mixed|null
     */
    public function __call($name, $arguments)
    {
        if (in_array($name, self::ALLOWED_METHODS)) {
            return $this->{$name.'Params'}[$arguments[0]] ?? ($arguments[1] ?? null);
        } elseif (
            strpos($name, 'All') !== false
            && in_array(
                $methodName = str_replace('All', '', $name),
                self::ALLOWED_METHODS
            )
        ) {
            return $this->{$methodName.'Params'};
        }

        return null;
    }
}