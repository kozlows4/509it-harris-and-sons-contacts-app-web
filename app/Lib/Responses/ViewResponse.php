<?php


namespace app\Lib\Responses;


use app\Interfaces\ExecutableResponseInterface;
use app\Lib\Response;

/**
 * Class ViewResponse
 *
 * @package app\Lib\Responses
 */
class ViewResponse implements ExecutableResponseInterface
{
    /**
     * @var int $status
     */
    private int $status;

    /**
     * @var string $header
     */
    private string $header;

    /**
     * @var string $view
     */
    private string $view;

    /**
     * @var array $data
     */
    private array $data;

    /**
     * ViewResponse constructor.
     *
     * @param string $view
     * @param array $data
     * @param int $status
     * @param string $header
     */
    public function __construct(
        string $view,
        array $data = [],
        int $status = Response::ROUTING_SUCCESS,
        string $header = 'Content-Type: text/html'
    )
    {
        $this->status = $status;
        $this->header = $header;
        $this->view = $view;
        $this->data = $data;
    }

    /**
     * @see ExecutableResponseInterface::execute()
     */
    public function execute(): void
    {
        http_response_code($this->status);
        header($this->header);

        extract($this->data);

        if (file_exists($file = '../resources/views/pages/' . $this->view)) {
            include '../resources/views/base.php';
        }
    }
}