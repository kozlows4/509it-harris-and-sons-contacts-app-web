<?php


namespace app\Lib\Responses;


use app\Interfaces\ExecutableResponseInterface;
use app\Lib\Response;

/**
 * Class JsonResponse
 *
 * @package app\Lib\Responses
 */
class JsonResponse implements ExecutableResponseInterface
{
    /**
     * @var int $status
     */
    private int $status;

    /**
     * @var array $data
     */
    private array $data;

    /**
     * JsonResponse constructor.
     *
     * @param array $data
     * @param int $status
     */
    public function __construct(array $data = [], int $status = Response::ROUTING_SUCCESS)
    {
        $this->status = $status;
        $this->data = $data;
    }

    /**
     * @see ExecutableResponseInterface::execute()
     */
    public function execute(): void
    {
        http_response_code($this->status);
        header('Content-Type: application/json');

        echo json_encode($this->data);
    }
}