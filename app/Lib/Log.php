<?php


namespace app\Lib;


use Monolog\Handler\StreamHandler;
use Monolog\Logger;

/**
 * Class Log
 *
 * @package app\Lib
 */
class Log
{
    /**
     * Instance of the class
     *
     * @var self|null
     */
    private static ?self $instance = null;

    /**
     * Logger instance
     *
     * @var Logger $logger
     */
    private Logger $logger;

    /**
     * Store error log
     *
     * @param string $message
     */
    public static function errorLog(string $message): void
    {
        self::getInstance()->logger->error($message);
    }

    /**
     * Get instance of the class
     *
     * @return static
     */
    private static function getInstance(): self
    {
        return is_null(self::$instance)
            ? (self::$instance = new self())
            : self::$instance;
    }

    /**
     * Log constructor.
     */
    private function __construct()
    {
        $this->logger = new Logger('log');
        $this->logger->pushHandler(new StreamHandler('../logs/error.log', Logger::ERROR));
    }
}