<?php

namespace app\Lib;

/**
 * Class App
 *
 * @package app\Lib
 */
class App
{
    /**
     * Run the application
     */
    public static function run(): void
    {
        session_start();

        Router::handleRouting(new Request());
    }
}