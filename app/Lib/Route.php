<?php


namespace app\Lib;

/**
 * Class Route
 *
 * @package app\Lib
 *
 * @method static self get(string $path, ?mixed $callback = null)
 * @method static self post(string $path, ?mixed $callback = null)
 */
class Route
{
    /**
     * @const
     */
    private const ALLOWED_METHODS = [
        'get',
        'post'
    ];

    /**
     * @var string $method
     */
    private string $method;

    /**
     * @var string $path
     */
    private string $path;

    /**
     * @var callable|string|null
     */
    private $callback;

    /**
     * @var string|null
     */
    private ?string $name = null;

    /**
     * @var string|null
     */
    private ?string $redirect = null;

    /**
     * Route constructor.
     *
     * @param string $method
     * @param string $path
     * @param string|callable|null $callback
     */
    public function __construct(string $method, string $path, $callback = null)
    {
        $this->method = $method;
        $this->path = $path;
        $this->callback = $callback;
    }

    /**
     * Add new route
     *
     * @param $name
     * @param $arguments
     * @return Route
     */
    public static function __callStatic($name, $arguments): ?Route
    {
        if (in_array($method = strtolower($name), self::ALLOWED_METHODS)) {
            $route = new self(
                $method,
                $arguments[0],
                $arguments[1] ?? null
            );

            Router::addRoute($route);

            return $route;
        } else {
            return null;
        }
    }

    /**
     * Get route's method
     *
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * Get route's path
     *
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * Get route's name
     *
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * Get route's redirect
     *
     * @return string|null
     */
    public function getRedirect(): ?string
    {
        return $this->redirect;
    }

    /**
     * Get route's callback
     *
     * @return callable|string|null
     */
    public function getCallback()
    {
        return $this->callback;
    }

    /**
     * Set route's name
     *
     * @param string $name
     * @return $this
     */
    public function name(string $name = ''): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Set route's redirect
     *
     * @param string $redirect
     * @return $this
     */
    public function redirect(string $redirect = ''): self
    {
        $this->redirect = $redirect;

        return $this;
    }
}