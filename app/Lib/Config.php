<?php

namespace app\Lib;

/**
 * Class Config
 * @package app\Lib
 */
class Config
{
    /**
     * @var array|null $config
     */
    private static ?array $config = null;

    /**
     * Get config value
     *
     * @param $key
     * @param mixed|null $default
     * @return mixed|null
     */
    public static function get(string $key, $default = null)
    {
        if (is_null(self::$config)) {
            self::$config = require_once(__DIR__ . '/../../config/config.php');
        }

        return !empty(self::$config[$key]) ? self::$config[$key] : $default;
    }
}