<?php


use app\Controllers\BusinessContactsController;
use app\Lib\Request;
use app\Lib\Responses\JsonResponse;
use app\Lib\Responses\ViewResponse;
use PHPUnit\Framework\TestCase;
use function PHPUnit\Framework\assertInstanceOf;

/**
 * Class BusinessContactsControllerTest
 */
class BusinessContactsControllerTest extends TestCase
{
    /**
     * @var BusinessContactsController $controller
     */
    private BusinessContactsController $controller;

    protected function setUp(): void
    {
        parent::setUp();

        $this->controller = new BusinessContactsController();
    }

    /**
     * Test getContacts method
     * @see BusinessContactsController::getContacts()
     */
    public function testGetContacts()
    {
        $response = $this->controller->getContacts(
            new Request(
                Request::POST,
                [
                    'search' => [
                        'value' => ''
                    ],
                    'columns' => [
                        [
                            'data' => 'id'
                        ]
                    ],
                    'order' => [
                        [
                            'column' => 0,
                            'dir' => ''
                        ]
                    ]
                ]
            )
        );

        assertInstanceOf(JsonResponse::class, $response);
    }

    /**
     * Test deleteContact method
     * @see BusinessContactsController::deleteContact()
     */
    public function testDeleteContact()
    {
        $response = $this->controller->deleteContact(
            new Request(
                Request::POST,
                [
                    'id' => 0
                ]
            )
        );

        assertInstanceOf(JsonResponse::class, $response);
    }

    /**
     * Test editContact method
     * @see BusinessContactsController::editContact()
     */
    public function testEditContact()
    {
        $response = $this->controller->editContact(
            new Request(
                Request::GET
            )
        );

        assertInstanceOf(ViewResponse::class, $response);
    }
}