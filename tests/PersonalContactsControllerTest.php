<?php


use app\Controllers\PersonalContactsController;
use app\Lib\Request;
use app\Lib\Responses\JsonResponse;
use app\Lib\Responses\ViewResponse;
use PHPUnit\Framework\TestCase;
use function PHPUnit\Framework\assertInstanceOf;

/**
 * Class PersonalContactsControllerTest
 */
class PersonalContactsControllerTest extends TestCase
{
    /**
     * @var PersonalContactsController $controller
     */
    private PersonalContactsController $controller;

    protected function setUp(): void
    {
        parent::setUp();

        $this->controller = new PersonalContactsController();
    }

    /**
     * Test getContacts method
     * @see PersonalContactsController::getContacts()
     */
    public function testGetContacts()
    {
        $response = $this->controller->getContacts(
            new Request(
                Request::POST,
                [
                    'search' => [
                        'value' => ''
                    ],
                    'columns' => [
                        [
                            'data' => 'id'
                        ]
                    ],
                    'order' => [
                        [
                            'column' => 0,
                            'dir' => ''
                        ]
                    ]
                ]
            )
        );

        assertInstanceOf(JsonResponse::class, $response);
    }

    /**
     * Test deleteContact method
     * @see PersonalContactsController::deleteContact()
     */
    public function testDeleteContact()
    {
        $response = $this->controller->deleteContact(
            new Request(
                Request::POST,
                [
                    'id' => 0
                ]
            )
        );

        assertInstanceOf(JsonResponse::class, $response);
    }

    /**
     * Test editContact method
     * @see PersonalContactsController::editContact()
     */
    public function testEditContact()
    {
        $response = $this->controller->editContact(
            new Request(
                Request::GET
            )
        );

        assertInstanceOf(ViewResponse::class, $response);
    }
}