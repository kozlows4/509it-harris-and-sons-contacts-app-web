<?php

use app\Lib\App;

require dirname(__DIR__, 1) . '/vendor/autoload.php';
require dirname(__DIR__, 1) . '/app/helpers.php';
require dirname(__DIR__, 1) . '/routes/web.php';

App::run();