<?php
    use app\Enums\MainMenuItems;
?>
<!--
    Used packages:
    Bootstrap 4
    jQuery
    DataTable
-->
<html lang="en">
<head>
    <title>
        Harris & Sons Consulting LTD - Contact Management
    </title>

    <meta charset="UTF-8">
    <link href="<?php __(mix('css/app.css'))?>" rel="stylesheet" type="text/css">
    <script src="<?php __(mix('js/app.js'))?>"></script>
</head>
<body>
<div class="container-fluid">
    <header class="row">
        Harris & Sons Consulting LTD - Contact Management App
    </header>
    <div class="row main-content">
        <nav class="main-menu">
            <a
                class="main-menu-item <?php __((($activeMenuItem ?? '') === MainMenuItems::MAIN_MENU) ? 'active' : '')?>"
                href="<?php __(route('root'))?>"
            >
                Main Menu
            </a>

            <a
                class="main-menu-item <?php __((($activeMenuItem ?? '') === MainMenuItems::BUSINESS_CONTACTS) ? 'active' : '')?>"
                href="<?php __(route('viewBusinessContacts'))?>"
            >
                Business Contacts
            </a>

            <a
                class="main-menu-item <?php __((($activeMenuItem ?? '') === MainMenuItems::PERSONAL_CONTACTS) ? 'active' : '')?>"
                href="<?php __(route('viewPersonalContacts'))?>"
            >
                Personal Contacts
            </a>

            <a
                class="main-menu-item <?php __((($activeMenuItem ?? '') === MainMenuItems::ADD_BUSINESS_CONTACT) ? 'active' : '')?>"
                href="<?php __(route('addBusinessContact'))?>"
            >
                Add Business Contact
            </a>

            <a
                class="main-menu-item <?php __((($activeMenuItem ?? '') === MainMenuItems::ADD_PERSONAL_CONTACT) ? 'active' : '')?>"
                href="<?php __(route('addPersonalContact'))?>"
            >
                Add Personal Contact
            </a>
        </nav>

        <main class="col content-container">
            <?php include $file ?? ''; ?>
        </main>
    </div>
    <footer class="row">
        &copy; 2020
    </footer>
</div>
</body>
</html>