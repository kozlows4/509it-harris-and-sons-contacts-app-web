<div class="row">
    <div class="col-12">
        <h1 class="display-1 page-title">
            Business Contacts
        </h1>

        <table class="data-table table thead-light table-bordered table-striped" id="businessContactsTable"></table>
    </div>
</div>

<script>
    $(function () {
        let table = $('#businessContactsTable').DataTable({
            serverSide: true,
            processing: true,
            dom: "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
                "<'row'<'col-sm-12 table-container'tr>>" +
                "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
            ajax: {
                url: '<?php __(route('getBusinessContacts')); ?>',
                method: 'POST'
            },
            columns: [
                {
                    title: 'id',
                    data: 'id',
                    visible: false,
                    sortable: false
                },
                {
                    title: 'First Name',
                    data: 'first_name',
                    sortable: true
                },
                {
                    title: 'Last Name',
                    data: 'last_name',
                    sortable: true
                },
                {
                    title: 'Email',
                    data: 'email',
                    sortable: true
                },
                {
                    title: 'Phone',
                    sortable: false,
                    render: function (data, type, row) {
                        if (type === 'display') {
                            if (row.phone_area_code === '0') {
                                return row.phone_area_code + row.phone_number;
                            } else {
                                return '+' + row.phone_area_code + '&nbsp;' + row.phone_number;
                            }
                        } else {
                            return '';
                        }
                    }
                },
                {
                    title: 'Address',
                    sortable: false,
                    render: function (data, type, row) {
                        if (type === 'display') {
                            let address = row.address_line_1;

                            if (row.address_line_2 && row.address_line_2.length > 0) {
                                address += '<br>' + row.address_line_2;
                            }

                            if (row.address_line_3 && row.address_line_3.length > 0) {
                                address += '<br>' + row.address_line_3;
                            }

                            return address;
                        } else {
                            return '';
                        }
                    }
                },
                {
                    title: 'Postcode',
                    data: 'postcode',
                    sortable: false
                },
                {
                    title: 'City',
                    data: 'city',
                    sortable: false
                },
                {
                    title: 'Country',
                    data: 'country',
                    sortable: false
                },
                {
                    title: 'Business Phone',
                    sortable: false,
                    render: function (data, type, row) {
                        if (type === 'display') {
                            if (row.business_phone_area_code === '0') {
                                return row.business_phone_area_code + row.business_phone_number;
                            } else {
                                return '+' + row.business_phone_area_code + '&nbsp;' + row.business_phone_number;
                            }
                        } else {
                            return '';
                        }
                    }
                },
                {
                    title: 'Company',
                    data: 'company',
                    sortable: true
                },
                {
                    title: 'Position',
                    data: 'position',
                    sortable: true
                },
                {
                    title: 'Actions',
                    sortable: false,
                    render: function (data, type, row) {
                        if (type === 'display') {
                            return DataTableButtons.renderLinkButton(
                                    '<?php __(route('editBusinessContact', ['contact' => '']));?>' + row.id,
                                    'Edit',
                                    'warning'
                                ) +
                                DataTableButtons.renderIdButton(
                                    row.id,
                                    'Delete',
                                    'danger',
                                    'delete-button'
                                );
                        } else {
                            return '';
                        }
                    }
                }
            ]
        }).on('click', '.delete-button', function () {
            if (confirm('Delete contact?')) {
                $.ajax({
                    method: 'POST',
                    url: '<?php __(route('deleteBusinessContact')); ?>',
                    data: {
                        id: this.getAttribute('data-id')
                    },
                    success: function (data) {
                        if (!data.delete) {
                            alert('Contact cannot be deleted');
                        } else {
                            table.ajax.reload(null, false);
                        }
                    },
                    error: function () {
                        alert('Contact cannot be deleted');
                    }
                });
            }
        });
    });
</script>