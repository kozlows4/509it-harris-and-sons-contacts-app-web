<div class="row">
    <div class="col-12">
        <h1 class="display-1 page-title">
            <?php __(($isEdit ?? false) ? 'Edit' : 'Add'); ?> Personal Contact
        </h1>

        <form class="contact-form" autocomplete="off" id="contactForm">
            <input
                type="hidden"
                name="id"
                value="<?php __($contact['id'] ?? ''); ?>"
            >

            <div class="form-row">
                <fieldset class="col">
                    <legend>
                        Contact Details
                    </legend>

                    <div class="form-group">
                        <label for="firstName">First Name</label>
                        <input
                            class="form-control"
                            type="text"
                            name="firstName"
                            id="firstName"
                            maxlength="50"
                            value="<?php __($contact['first_name'] ?? ''); ?>"
                            required
                        >
                    </div>

                    <div class="form-group">
                        <label for="lastName">Last Name</label>
                        <input
                            class="form-control"
                            type="text"
                            name="lastName"
                            id="lastName"
                            maxlength="50"
                            value="<?php __($contact['last_name'] ?? ''); ?>"
                            required
                        >
                    </div>

                    <div class="form-group">
                        <label for="email">Email</label>
                        <input
                            class="form-control"
                            type="email"
                            name="email"
                            id="email"
                            maxlength="255"
                            value="<?php __($contact['email'] ?? ''); ?>"
                            required
                        >
                    </div>

                    <div class="form-group">
                        <label for="phoneAreaCode">Phone Number Area Code</label>
                        <input
                            class="form-control"
                            type="text"
                            name="phoneAreaCode"
                            id="phoneAreaCode"
                            maxlength="5"
                            value="<?php __($contact['phone_area_code'] ?? ''); ?>"
                            required
                        >
                    </div>

                    <div class="form-group">
                        <label for="phoneNumber">Phone Number</label>
                        <input
                            class="form-control"
                            type="text"
                            name="phoneNumber"
                            id="phoneNumber"
                            maxlength="20"
                            value="<?php __($contact['phone_number'] ?? ''); ?>"
                            required
                        >
                    </div>
                </fieldset>

                <fieldset class="col">
                    <legend>
                        Address
                    </legend>

                    <div class="form-group">
                        <label for="addressLine1">Address Line 1</label>
                        <input
                            class="form-control"
                            type="text"
                            name="addressLine1"
                            id="addressLine1"
                            maxlength="255"
                            value="<?php __($contact['address_line_1'] ?? ''); ?>"
                            required
                        >
                    </div>

                    <div class="form-group">
                        <label for="addressLine2">Address Line 2</label>
                        <input
                            class="form-control"
                            type="text"
                            name="addressLine2"
                            id="addressLine2"
                            maxlength="255"
                            value="<?php __($contact['address_line_2'] ?? ''); ?>"
                        >
                    </div>

                    <div class="form-group">
                        <label for="addressLine3">Address Line 3</label>
                        <input
                            class="form-control"
                            type="text"
                            name="addressLine3"
                            id="addressLine3"
                            maxlength="255"
                            value="<?php __($contact['address_line_3'] ?? ''); ?>"
                        >
                    </div>

                    <div class="form-group">
                        <label for="postcode">Postcode</label>
                        <input
                            class="form-control"
                            type="text"
                            name="postcode"
                            id="postcode"
                            maxlength="10"
                            value="<?php __($contact['postcode'] ?? ''); ?>"
                            required
                        >
                    </div>

                    <div class="form-group">
                        <label for="city">City</label>
                        <input
                            class="form-control"
                            type="text"
                            name="city"
                            id="city"
                            maxlength="255"
                            value="<?php __($contact['city'] ?? ''); ?>"
                            required
                        >
                    </div>

                    <div class="form-group">
                        <label for="country">Country</label>
                        <input
                            class="form-control"
                            type="text"
                            name="country"
                            id="country"
                            maxlength="255"
                            value="<?php __($contact['country'] ?? ''); ?>"
                            required
                        >
                    </div>
                </fieldset>

                <fieldset class="col">
                    <legend>
                        Personal Contact Details
                    </legend>

                    <div class="form-group">
                        <label for="homePhoneAreaCode">Home Phone Number Area Code</label>
                        <input
                            class="form-control"
                            type="text"
                            name="homePhoneAreaCode"
                            id="homePhoneAreaCode"
                            maxlength="5"
                            value="<?php __($contact['home_phone_area_code'] ?? ''); ?>"
                            required
                        >
                    </div>

                    <div class="form-group">
                        <label for="homePhoneNumber">Home Phone Number</label>
                        <input
                            class="form-control"
                            type="text"
                            name="homePhoneNumber"
                            id="homePhoneNumber"
                            maxlength="20"
                            value="<?php __($contact['home_phone_number'] ?? ''); ?>"
                            required
                        >
                    </div>
                </fieldset>
            </div>
            <div class="form-row">
                <input
                    type="submit"
                    value="<?php __(($isEdit ?? false) ? 'Update' : 'Add'); ?> Contact"
                    class="btn btn-primary mx-auto"
                >
            </div>
        </form>
    </div>
</div>

<?php
    $route = route('viewPersonalContacts');

    include '../resources/views/components/modals/after_add_edit_modal.php';
?>

<script>
    $(function () {
        $('#contactForm').on('submit', function (event) {
            event.preventDefault();

            if (confirm('<?php __(($isEdit ?? false) ? 'Update' : 'Add'); ?> contact?')) {
                $.ajax({
                    method: 'POST',
                    url: '<?php __(route('storePersonalContact')); ?>',
                    data: new FormData(
                        document.getElementById('contactForm')
                    ),
                    processData: false,
                    contentType: false,
                    success: function (data) {
                        if (data.store) {
                            $('#contactModal').modal('show');
                        } else {
                            alert('Contact cannot be saved');
                        }
                    },
                    error: function () {
                        alert('Contact cannot be saved');
                    }
                });
            }
        });
    });
</script>