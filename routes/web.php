<?php

use app\Enums\MainMenuItems;
use app\Lib\Response;
use app\Lib\Route;

#region Root route
Route::get('/', function () {
    (new Response())
        ->view('home.php', [
            'activeMenuItem' => MainMenuItems::MAIN_MENU
        ])
        ->execute();
})->name('root');
#endregion Root route

#region Business contacts routes
Route::get('/businessContacts', function () {
    (new Response())
        ->view('business_contacts/business_contacts_list.php', [
            'activeMenuItem' => MainMenuItems::BUSINESS_CONTACTS
        ])
        ->execute();
})->name('viewBusinessContacts');

Route::get('/addBusinessContact', function () {
    (new Response())
        ->view('business_contacts/business_contact_add_edit.php', [
            'activeMenuItem' => MainMenuItems::ADD_BUSINESS_CONTACT
        ])
        ->execute();
})->name('addBusinessContact');

Route::post('/getBusinessContacts', 'BusinessContactsController@getContacts')
    ->name('getBusinessContacts');

Route::post('/deleteBusinessContact', 'BusinessContactsController@deleteContact')
    ->name('deleteBusinessContact');

Route::get('/editBusinessContact', 'BusinessContactsController@editContact')
    ->name('editBusinessContact');

Route::post('/storeBusinessContact', 'BusinessContactsController@storeContact')
    ->name('storeBusinessContact');
#endregion Business contacts routes

#region Personal contacts routes
Route::get('/personalContacts', function () {
    (new Response())
        ->view('personal_contacts/personal_contacts_list.php', [
            'activeMenuItem' => MainMenuItems::PERSONAL_CONTACTS
        ])
        ->execute();
})->name('viewPersonalContacts');

Route::get('/addPersonalContact', function () {
    (new Response())
        ->view('personal_contacts/personal_contact_add_edit.php', [
            'activeMenuItem' => MainMenuItems::ADD_PERSONAL_CONTACT
        ])
        ->execute();
})->name('addPersonalContact');

Route::post('/getPersonalContacts', 'PersonalContactsController@getContacts')
    ->name('getPersonalContacts');

Route::post('/deletePersonalContact', 'PersonalContactsController@deleteContact')
    ->name('deletePersonalContact');

Route::get('/editPersonalContact', 'PersonalContactsController@editContact')
    ->name('editPersonalContact');

Route::post('/storePersonalContact', 'PersonalContactsController@storeContact')
    ->name('storePersonalContact');
#endregion Personal contacts routes

